<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

/**
 * Possibilité de nettoyer l'url au besoin
 *
 * @param $url  Url du fichier XML à analyser.
 *
 * @return string
 *          Retourne l'url formaté
 */
function inc_nettoyer_url_revision_dist($url) {
	return $url;
}
