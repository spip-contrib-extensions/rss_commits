<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'champ_versioning_path_label' => 'Chemin du dépôt',
	'champ_versioning_rss_label' => 'RSS des commits du dépôt',
	'champ_versioning_trac_label' => 'Trac du dépôt',
	'champ_versioning_type_label' => 'Type de versioning',
	// E
	'explication_versioning_path' => '-',
	'explication_versioning_rss' => '-',
	'explication_versioning_trac' => '-',
	'explication_versioning_type' => '-',

);
