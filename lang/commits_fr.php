<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'commits_titre' => 'Commits de projet',
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',

	// E
	'explication_curl' => 'Veuillez indiquer le login et le mot de passe utilisés pour se connecter sur les pages du webservice. Ces informations seront utilisées avec la fonction CURL de PHP si elle est présente sur votre site.',
	'explication_import_auto' => 'Il peut s\'avérer parfois très volumineux d\'importer les commits de tous les projets dans la base de données. De ce fait, vous pouvez désactiver cette option selon vos besoins et/ou envie. Par défaut, cette option est désactivée.',
	'alerte_import_auto_desactive' => 'Attention, l\'importation automatique des commits dans la base de données a été désactivée. Les remontées de nouveaux commits se feront par le biais des flux RSS indiqués sur les projets.',

	// L
	'label_login' => 'Login',
	'label_password' => 'Mot de passe',
	'label_import_auto' => 'Importation automatique',

	// N
	'non' => 'Non',

	// O
	'oui' => 'Oui',
	// T
	'titre_page_configurer_commits' => 'Régler vos commits',
);

